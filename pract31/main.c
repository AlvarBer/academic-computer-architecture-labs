/*-------------------------------------------------------------------
**
**  Fichero:
**    main.c  10/6/2014
**
**    Estructura de Computadores
**    Dpto. de Arquitectura de Computadores y Autom�tica
**    Facultad de Inform�tica. Universidad Complutense de Madrid
**
**  Prop�sito:
**    Implementa el juego del mastermind
**
**  Notas de dise�o:
**
**-----------------------------------------------------------------*/

#include "44b.h"
#include "utils.h"
#include "leds.h"
#include "D8Led.h"
#include "timer.h"
#include "keyboard.h"
#include "globals.h"

void intcont_init(void) {
	rINTCON = 0x1; //Vectorizado, IRQ, FIQ habilitadas
	rINTMOD = 0x0; //IRQ todas
	rI_ISPC = 0x3ffffff; //Borramos pendientes IRQ
	rF_ISPC = 0x3ffffff; //Borramos pendientes FIQ
	rEXTINTPND = 0x0; //Borramos EXTINTPND
	rINTMSK = 0x3FFFFFF; //BITGLOBAL enmascaramos todo
	rINTMSK &= ~BIT_GLOBAL;
}

void constant_init( void ) {
	secretCode = 0;
	introducedCode = 0;
	passwordIntroduced = 0;
	D8Led_digit(0xC);
}


int main(void) {
	intcont_init();
	D8Led_init();
	keyboard_init();
	timer_init();
	constant_init();

	Delay(0);
		
	while(1)
	return 0;
}
