/*-------------------------------------------------------------------
**
**  Fichero:
**    timer.h  10/6/2014
**
**    Estructura de Computadores
**    Dpto. de Arquitectura de Computadores y Automática
**    Facultad de Informática. Universidad Complutense de Madrid
**
**  Propósito:
**    Contiene las definiciones de los prototipos de funciones
**    para la gestión del timer 0 del chip S3C44BOX
**
**  Notas de diseño:
**
**-----------------------------------------------------------------*/

#include "44b.h"
#include "leds.h"
#include "D8led.h"
#include "timer.h"
#include "keyboard.h"
#include "globals.h"

#define TIMER1_MODE (0x1<<11)
#define TIMER1_MANUAL_UPDATE (0x1<<9)
#define TIMER1_STARTSTOP (0x1<<8)

#define TIMER1_CFG1_1_8_MASK 0x20
#define TIMER1_CFG0_PREESC_255_MASK 0xFF

static int symbol = 0;     /* variable de estado del símbolo del contador software */
static int timer_stat = 0; /* variable de estado del timer (1 activo, 0 inactivo)   */

static void timer_ISR(void) __attribute__ ((interrupt ("IRQ")));

void timer_start(void) {
    /* manual update */
	rTCON |= TIMER1_MANUAL_UPDATE;
	/* Se borra el bit de interrupción pendiente del TIMER2 */
	rI_ISPC |= BIT_TIMER1;
	/* not manual update, start and auto-reload*/
	rTCON &= ~TIMER1_MANUAL_UPDATE;
	rTCON |= (TIMER1_STARTSTOP | TIMER1_MODE);
	/* Se borra el bit de interrupción pendiente del TIMER2 */
	rI_ISPC |= BIT_TIMER1;

	timer_stat = 1;
	/* Desenmascara la linea del timer 2 y el bit global */
	rINTMSK &= ~(BIT_TIMER1 | BIT_GLOBAL);
}

void timer_stop(void) {
    /* manual update */
	//rTCON |= TIMER2_MANUAL_UPDATE;
	/* Se borra el bit de interrupción pendiente del TIMER2 */
	rI_ISPC |= BIT_TIMER1;
	/* not manual update, stop and auto-reload*/
	//rTCON &= ~(TIMER2_MANUAL_UPDATE | TIMER2_STARTSTOP);
	rTCON &= ~TIMER1_STARTSTOP;
	rTCON |= TIMER1_MODE;
	/* Se borra el bit de interrupción pendiente del TIMER2 */
	rI_ISPC |= BIT_TIMER1;

	rINTMSK |= (BIT_TIMER1);

	timer_stat = 0;
}

void timer_clear(void) {
	timer_stop();
	symbol = 0;
	D8Led_digit(symbol);
}


static void timer_ISR( void ) {
	if (passwordIntroduced == 0) { //If it hasn't been introduced
		passwordIntroduced = 1;
		D8Led_digit(0xF);
	} else if (passwordIntroduced == 1) { //If it has
		if (passwordIsCorrect() == 0) { //If it has succeeded
			D8Led_digit(0xA);
		} else { //If it failed
			D8Led_digit(0xE);
		}
		timer_start();
	}
	rI_ISPC = 0x07FFFFFF;
}

void timer_init(void) {
	// Establecer la rutina de servicio para TIMER2
		pISR_TIMER1 = timer_ISR;

	   /* f = MCLK / ((P+1) * D)
	    * para una interrupción cada 2 segundos: N * 1/f = 2 con N <= 65535
		* N * ((P+1)*D) / MCLK = 2 => P = 2 * MCLK / (N*D) - 1
		* Tomamos N como el mayor divisor de 2*CLK/D => para D = 8 y MCLK = 64MHz, N = 65200
	    * y P = 2*64000000/(62500*8) - 1 = 255, que cabe justo en los 8 bits de pre-escalado
	    */
		/* divisor 1/8 */
		rTCFG1 |= TIMER1_CFG1_1_8_MASK;
		/* pre-escalado 255 */
		rTCFG0 |= TIMER1_CFG0_PREESC_255_MASK;

		//Initial values
		rTCNTB1 = 62500;
		rTCMPB1 = 0;

		//Manual load
		rTCON |= TIMER1_MANUAL_UPDATE;
		//Start with autoreload
		rI_ISPC |= BIT_TIMER1;
		//Not manual update, start and autoreload
		rTCON &= ~TIMER1_MANUAL_UPDATE;
		rTCON |= (TIMER1_STARTSTOP | TIMER1_MODE);

		//Se borra el bit de interrupcion
		rI_ISPC |= BIT_TIMER1;

		/* Dejar el timer parado */
		timer_clear();

		timer_start();
}
