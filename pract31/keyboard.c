/*-------------------------------------------------------------------
**
**  Fichero:
**    keyboard.c  10/6/2014
**
**    Estructura de Computadores
**    Dpto. de Arquitectura de Computadores y Autom�tica
**    Facultad de Inform�tica. Universidad Complutense de Madrid
**
**  Prop�sito:
**    Contiene las implementaciones de las funciones
**    para la gesti�n del teclado de la placa de prototipado
**
**  Notas de dise�o:
**
**-----------------------------------------------------------------*/

#include "44b.h"
#include "utils.h"
#include "keyboard.h"
#include "D8Led.h"
#include "globals.h"

#define KEY_VALUE_MASK	0x0f
#define GPORT_EINT1 0x0C

//static int key_read( void );
static void keyboard_ISR(void) __attribute__ ((interrupt ("IRQ")));

int secretCode;
int introducedCode;
int passwordIntroduced;
int displaying;

void keyboard_init( void ) {
	/* Configurar puerto G para interrupciones del teclado */
	rPCONG |= GPORT_EINT1;

	/* Establecer ISR de teclado */
	pISR_EINT1 = keyboard_ISR;

	/*Borrar interrupciones antes habilitar*/
	rI_ISPC = 0x07FFFFFF;
	rF_ISPC = 0x3FFFFFF;

    /* Desenmascara la l�nea del teclado y el bit global */
	rINTMSK &= ~(BIT_EINT1 | BIT_GLOBAL);
}

char key_read( void ) {
	char value;
	char temp;

	/* leer linea 1 */
	temp = *(KEYBADDR+0xfd);
	temp = temp & KEY_VALUE_MASK;
	if (temp  != KEY_VALUE_MASK) {
		if( temp == 0x0E )
			value = 3;
		else if( temp == 0x0D )
			value = 2;
		else if( temp == 0x0B )
			value = 1;
		else if( temp == 0x07 )
			value = 0;
		return value;
	}
	
	/* linea 2 */
	temp = *(KEYBADDR+0xfb);
	temp = temp & KEY_VALUE_MASK;
	if (temp  != KEY_VALUE_MASK) {
		if( temp == 0x0E )
			value = 7;
		else if( temp == 0x0D )
			value = 6;
		else if( temp == 0x0B )
			value = 5;
		else if( temp == 0x07 )
			value = 4;
		return value;
	}
	
	/* linea 3 */
	temp = *(KEYBADDR+0xf7);
	temp = temp & KEY_VALUE_MASK;
	if (temp  != KEY_VALUE_MASK) {
		if( temp == 0x0E )
			value = 0xb;
		else if( temp == 0x0D )
			value = 0xa;
		else if( temp == 0x0B )
			value = 9;
		else if( temp == 0x07 )
			value = 8;
		return value;
	}
	
	/* linea 4 */
	temp = *(KEYBADDR+0xef);
	temp = temp & KEY_VALUE_MASK;
	if (temp  != KEY_VALUE_MASK) {
		if( temp == 0x0E )
			value = 0xf;
		else if( temp == 0x0D )
			value = 0xe;
		else if( temp == 0x0B )
			value = 0xd;
		else if( temp == 0x07 )
			value = 0xc;
		return value;
	}
	return -1;
}

static void keyboard_ISR(void) {
	char key;

	/* Eliminar rebotes de presion */
	Delay(2000);

	/* Escaneo de tecla */
	key = key_read();

	// En caso de error, key = -1
	if(key != -1 && key < 0x20 && displaying != 0) {
		if (key == 0xF) { //If the user presses F
			displaying = 0;
			if (passwordIntroduced == 0) { //If it hasn't been introduced
				passwordIntroduced = 1;
				D8Led_digit(0xF);
			} else if (passwordIntroduced == 1) { //If it has
				if (passwordIsCorrect() == 0) { //If it has succeeded
					D8Led_digit(0xA);
				} else { //If it failed
					D8Led_digit(0xE);
				}
			timer_start();
			}
		}
		else {
			if (passwordIntroduced == 0) { //Digit is created for outputting
				int digit = secretCode;
				digit = (digit << 4) + key;
				secretCode = digit;
				D8Led_digit(digit);
			} else if (passwordIntroduced == 1) {
				int digit = introducedCode;
				digit = (digit << 4) + key;
				introducedCode = digit;
				D8Led_digit(digit);
			}
		}
		secretCode = (secretCode << 4) + key;
	}

	/* Esperar a que la tecla se suelte */
	while (!(rPDATG & 0x02));

    /* Eliminar rebotes de depreson */
    Delay(2000);

    /* Borrar interrupciones pendientes */
    rI_ISPC |= BIT_EINT1;
}

int passwordIsCorrect(void) {
	int key = secretCode & 0xFFFF;
	int try = introducedCode & 0xFFFF;
	return (key == try ? 0 : 1);
}
