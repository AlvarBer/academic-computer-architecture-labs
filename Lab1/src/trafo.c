#include "trafo.h"
#include "types.h"

void RGB2GrayMatrix(pixelRGB orig[], unsigned char dest[], int nfilas, int ncols) {
    int i, j;

    for (i = 0; i < nfilas; i++) {
    	for (j = 0; j < ncols; j++) {
    		dest[i * ncols + j] = rgb2gray(&orig[i * ncols + j]);
    	}
    }
}

void Gray2BinaryMatrix(unsigned char orig[], unsigned char dest[], unsigned char umbral, int nfilas, int ncols) {
	int i,j;
	for (i=0; i<nfilas; i++) {
		for (j=0; j<ncols; j++) {
			if (orig[i*ncols + j] > umbral) {
				dest[i*nfilas + j] = 255;
			}
			else {
				dest[i*nfilas + j] = 0;
			}
		}
	}
}

unsigned char rgb2gray(pixelRGB* pixel) {
	unsigned short int aux_r, aux_g, aux_b;
	aux_r = 3483*pixel->R;
	aux_g = 11718*pixel->G;
	aux_b = 1183*pixel->B;

	return (aux_r + aux_g + aux_b)/16384;
}

void computeHistogram(unsigned char imagenGris[], short int histogram[], int N, int M) {
	unsigned char umbral;

	int i,j;
	// Initialize histogram
	for (i=0; i<256; i++) {
		histogram[i]=0;
	}
	for (i=0; i<N; i++) {
		for (j=0;j<M;j++) {
			histogram[imagenGris[i*M + j]]++;
		}
	}

	umbral = computeThreshold(histogram);
}

unsigned char computeThreshold(short int histogram[]) {
	short int max=-1, max2=-1;
	int max_idx=-1, max2_idx=-1;
	int i;
	for (i=0;i<128;i++) {
		if (histogram[i] > max) {
			max = histogram[i];
			max_idx = i;
		}
	}
	for (i=128;i<255;i++) {
		if (histogram[i] > max2) {
			max2 = histogram[i];
			max2_idx = i;
		}
	}

	return max_idx + (max2_idx - max_idx)/2;
}

void contarUnos(unsigned char mat[], short int vector[], int nfilas, int ncols) {
	int i, j;

	//Initialize vector
	for (i = 0; i < nfilas; ++i) {
		vector[i] = 0;
	}

	for (i = 0; i < nfilas; ++i) {
		for (j = 0; j < ncols; ++j) {
			if (mat[i * ncols + j] == 255) {
				++vector[i];
			}
	    }
	}
}
